<?php 
/*----------------------------------------------------------------*\

	HTML HEAD CONTENT
	Commonly contains site meta data and tracking scripts.
	External resources are not referanced here, refer to the functions.php

\*----------------------------------------------------------------*/
?>

<!doctype html>
<html xml:lang="en" lang="en">

<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<a id="skip-to-content" href="#main-content">Skip to main content</a>

	<?php get_template_part('template-parts/icon-set'); ?>