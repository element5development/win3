<?php 
/*----------------------------------------------------------------*\

	ERROR / NO PAGE FOUND

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="post-head">
	<h1><?php echo "Oops! That page can't be found."; ?></h1>
</header>

<main id="main-content">
	<article>
		<p>It looks like nothing was found at this location. Maybe try a search?</p>
		<?php get_search_form(); ?>
	</article>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>