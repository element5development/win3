<?php
/*----------------------------------------------------------------*\
		INITIALIZE WIDGET AREA
\*----------------------------------------------------------------*/
function footer_widget_areas() {
	$args = array(
		'name'          => __( 'Footer' ),
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	);
	register_sidebar($args);
}
add_action( 'widgets_init', 'footer_widget_areas' );