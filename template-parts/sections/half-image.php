<?php 
/*----------------------------------------------------------------*\

		Half Image Section

\*----------------------------------------------------------------*/
?>

<section class="half-image blue">
	<div>
		<div>
			<h3><?php the_field('blue_title'); ?></h3>
			<p><?php the_field('blue_description'); ?></p>
			<p><a class="button is-secondary" href="<?php echo get_site_url(); ?>/#contact-us">Contact us</a></p>
		</div>
	</div>
</section>
<section class="half-image">
	<div>
		<div>
			<h3><?php the_field('half_title'); ?></h3>
			<p><?php the_field('half_description'); ?></p>
			<p><a class="button" href="<?php echo get_site_url(); ?>/#contact-us">Contact us</a></p>
		</div>
	</div>
</section>