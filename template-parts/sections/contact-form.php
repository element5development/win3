<?php 
/*----------------------------------------------------------------*\

		Contact Form Section

\*----------------------------------------------------------------*/
?>

<section class="contact-form">
	<?php $image = get_field('contact_image'); ?>
	<img src="<?php echo $image['sizes']['small']; ?>" alt="<?php echo $image['alt']; ?>" />
	<div id="contact-us">
		<?php echo do_shortcode('[gravityform id="5" title="true" description="false"]') ?>
	</div>
</section>