<?php 
/*----------------------------------------------------------------*\

		POST FOOTER
		Display copyright and navigation

\*----------------------------------------------------------------*/
?>

<footer class="post-footer">
	<div>
		<a class="logo" href="<?php echo get_home_url(); ?>">
			<svg>
				<use xlink:href="#logo-orange" />
			</svg>
		</a>
		<a class="memberships" target="_blank" href="https://www.napeo.org/">
			<img src="<?php echo get_template_directory_uri(); ?>/dist/images/napeo.svg" alt="NAPEO member"/>
		</a>
	</div>
	<div class="copyright">
		<p>© <?php echo date('Y'); ?> <?php echo get_bloginfo( 'name' ); ?>. All rights reserved. <a href="<?php echo get_home_url(); ?>/privacy-policy/">Privacy Policy</a><a href="<?php the_field('linkedin', 'option'); ?>" target="_blank">LinkedIn</a></p>
		<div id="element5-credit" data-emergence="hidden">
			<a target="_blank" href="https://element5digital.com">
				<img src="https://element5digital.com/wp-content/themes/e5-starting-point/dist/images/element5_credit_alt.svg" alt="Crafted by Element5 Digital" />
			</a>
		</div>
	</div>
</footer>