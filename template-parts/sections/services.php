<?php 
/*----------------------------------------------------------------*\

		Services Repeater Section

\*----------------------------------------------------------------*/
?>

<?php	if( have_rows('services_repeater') ): ?>
<div style="overflow: hidden;padding: 6em 0 8em;">
	<section class="services">
		<h2><?php the_field('services_title'); ?></h2>

		<?php	while ( have_rows('services_repeater') ) : the_row(); ?>
			<div>
				<?php $image = get_sub_field('icon'); ?>
				<img src="<?php echo $image['sizes']['small']; ?>" alt="<?php echo $image['alt']; ?>" />

				<h6><?php	the_sub_field('service'); ?></h6>
				<p><?php	the_sub_field('description'); ?></p>
			</div>
		<?php	endwhile; ?>
		<p><a class="button" href="<?php echo get_site_url(); ?>/#contact-us">Contact us</a></p>

	</section>
</div>
<?php	endif; ?>