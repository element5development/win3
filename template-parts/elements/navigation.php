<?php 
/*----------------------------------------------------------------*\

	PRIMARY NAVIGATION

\*----------------------------------------------------------------*/
?>
<?php 
	$phone_link = get_field('primary_phone_number','options');
	$phone_link = preg_replace('/[^0-9]/', '', $phone_link);
?>
<div class="primary-navigation">
	<nav>
		<a href="<?php echo get_home_url(); ?>">
			<svg>
				<use xlink:href="#logo" />
			</svg>
		</a>
		<a class="button" href="tel:+1<?php echo $phone_link; ?>">(844) 983-WIN3</a>
	</nav>
</div>