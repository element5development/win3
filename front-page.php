<?php 
/*----------------------------------------------------------------*\

	HOME/FRONT PAGE TEMPLATE
	Customized home page commonly composed of various reuseable sections.

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<main id="main-content">
	<article>
		<?php the_content(); ?>
	</article>
</main>

<?php get_template_part('template-parts/sections/half-image'); ?>

<?php get_template_part('template-parts/sections/services'); ?>

<?php get_template_part('template-parts/sections/contact-form'); ?>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>