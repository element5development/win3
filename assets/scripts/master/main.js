var $ = jQuery;

$(document).ready(function () {
	/*----------------------------------------------------------------*\
  	ENTRANCE ANIMATIONS
	\*----------------------------------------------------------------*/
	emergence.init({
		offsetTop: 20,
		offsetRight: 20,
		offsetBottom: 20,
		offsetLeft: 20,
	});

	/*------------------------------------------------------------------
  	STICKY NAV
	------------------------------------------------------------------*/
	$(window).scroll(function () {
		var scroll = $(window).scrollTop();
		if (scroll > 0) {
			$('.primary-navigation').addClass('is-stuck');
		} else {
			$('.primary-navigation').removeClass('is-stuck');
		}
	});
});